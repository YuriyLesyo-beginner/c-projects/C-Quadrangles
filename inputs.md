## 1 

**Write A**

[0, 0]

**Write B**

[7, 0]

**Write C**

[3, 2]

[10,2] ****parallelogram****

[-4,2] ****parallelogram****

[4,-2] ****parallelogram****

## 2

**Write A**

[0,0]

**Write B**

  [  5  ,  0     ]    
**Write C**

[3,

4

]

[8,4] **rhombus**

[-2,4] ****parallelogram****

[2,-4] ****parallelogram****

## 3


**Write A:**

[0,0]


**Write B:**

[-3,4]

**Write C:**

[4,3]

A': [1,7], **square**

B': [7,-1], **parallelogram**

C': [-7,1], **parallelogram**
## 4

**Write A:**

[10.5, 10.5] 	 [12.5, 10.5][10.5, 15e+0]


**Write B:**


**Write C:**

A': [12.5,15], **rectangle**

B': [8.5,15], **parallelogram**

C': [12.5,6], **parallelogram**
## 5

**Write A:**

[0, 0]


**Write B:**

[3, 3]


**Write C:**

[10, 10]

**Quadrangles cannot be created.**


**Write A:**

[0, 0]


**Write B:**

[2270.242, 0]


**Write C:**

[234.08, 2258.142]

A': [2504.322,2258.142], **rhombus**

B': [-2036.162,2258.142], **parallelogram**

C': [2036.162,-2258.142], **parallelogram**


**Write A:**

[740.865, 887.560]


**Write B:**

[340.090, 1241.872]


**Write C:**

[1095.177, 1288.335]


A': [694.402,1642.647], **square**

B': [1495.952,934.023], **parallelogram**


C': [-14.222,841.097], **parallelogram**
## 6

**Write A:**

[-306.710, -894.018]


**Write B:**

[6369.015, 66159.129]


**Write C:**

[6016.590, 62619.258]

**Quadrangles cannot be created.**
## 7

**Write A:**

[2, 5]


**Write B:**

[3, abcd]

**Input is not correct**


**Write A:**

[2, 5]


**Write B:**

[3, 4]

**Write C:**

[7 9]

**Input is not correct**