#include <stdio.h>
#include <math.h>
#include <float.h>

// creating an boolean type (just for me. Is not mandatory)
#define true  0;
#define false 1;

//----------------------------------------------------------------------------------------------------------------------
/// Represent structure of coordinates
typedef struct { double x, y; } coordinates;
//----------------------------------------------------------------------------------------------------------------------
/// Create an coordinate's object. Works like constructor
coordinates newCoordinates(double x, double y)
{
    coordinates newCoord; // initializing of structure

    newCoord.x = x; // adding value
    newCoord.y = y;

    return newCoord;
}
//----------------------------------------------------------------------------------------------------------------------
/// Find center between two points
coordinates findCenter(coordinates A, coordinates B)
{
    return newCoordinates(
                (A.x + B.x)/2,
                (A.y + B.y)/2
            );
}
//----------------------------------------------------------------------------------------------------------------------
/// Find points that we are looking for
coordinates findNextPoint(coordinates A, coordinates Center)
{
    return newCoordinates(
                2*Center.x - A.x,
                2*Center.y - A.y
            );
}
//----------------------------------------------------------------------------------------------------------------------
/// Return length between two points
double getLength(coordinates A, coordinates B)
{
    return sqrt(
            fabs(
                pow(B.x - A.x, 2)
                +
                pow(B.y - A.y, 2)
            )
    );
}
//----------------------------------------------------------------------------------------------------------------------
/// Check length between points and angle. Print type of figure
void printTypeOfFigure(coordinates cordOne,
                       coordinates cordTwo,
                       coordinates cordThree )
{
    /*           [0 | A]-----------------[1 | B]
     *              |                       |
     *              |                       |
     *              |                       |
     *              |                       |
     *           [3 | D]-----------------[2 | C]
     */

    double AB = getLength( cordOne , cordTwo ),
           BC = getLength( cordTwo , cordThree );


    if (    (cordTwo.x - cordOne.x)*(cordThree.x - cordOne.x) +
            (cordTwo.y - cordOne.y)*(cordThree.y - cordOne.y) <= 0.1)
        printf(AB == BC ? "square" : "rectangle");
    else
        printf(AB == BC ? "rhombus" : "parallelogram");

    printf("\n");
}
//----------------------------------------------------------------------------------------------------------------------
/// Check if the all coordinates are not in the same line
int checkCoords(coordinates * coord)
{
    if (
            (
            coord[0].x == coord[1].x == coord[2].x &&
            coord[0].y == coord[1].y == coord[2].y
    ) ||
    fabs(
            fabs(
                    (coord[2].x - coord[0].x) /
                    (coord[1].x - coord[0].x)
            ) -
            fabs(
                    (coord[2].y - coord[0].y) /
                    (coord[1].y - coord[0].y)
            )
    ) <= 100 * DBL_EPSILON * fabs(
            ( coord[2].x - coord[0].x ) /
            ( coord[1].x - coord[0].x )
    )
            )
    {
        printf("Quadrangles cannot be created\n");
        return false;
    }

    return true;
}
//----------------------------------------------------------------------------------------------------------------------
/// Print coordinate in format [x,y]
void printCoordinates(coordinates C) { printf("[%g,%g] ", C.x, C.y); }
//----------------------------------------------------------------------------------------------------------------------
/// Ask to write coordinates. Create and return new coordinate that readed from input
int readPoint(char pointName, coordinates * coord)
{
    printf("Write %c\n", pointName); // asking for writing

    // reading in format [x,y]. Ignore whitespaces. Checking
    if ( scanf(" [ %lf , %lf ]", &coord->x, &coord->y) != 2 )
    {
        printf("Input is not correct\n"
               "Coordinates should be writen in the format \"[x,y]\"\n");
        return false;
    }

    return true; // returning
}
//----------------------------------------------------------------------------------------------------------------------

int main(void)
{
    // initialing array with point`s name and coordinates
    char points[3] = {'A', 'B', 'C'};
    coordinates allCoords[3];

    for (int i = 0 ; i < 3 ; i++) // reading coordinates
        if ( readPoint( points[i],
                        &allCoords[i] ) )
            return 1;

    // checking if coordination are not in the same line and we can build figure
    if ( checkCoords(allCoords) )return 1;

    for (int i = 0 ; i < 3 ; i++) // printing
    {
        printCoordinates(
                findNextPoint( allCoords[i],
                               findCenter(
                                       allCoords[ (i+1)%3 ],
                                       allCoords[ (i+2)%3 ]
                               )
                     )
                );
        printTypeOfFigure(allCoords[ (i+1)%3 ], allCoords[ i ], allCoords[ (i+2)%3 ] );
    }
    return 0;
}