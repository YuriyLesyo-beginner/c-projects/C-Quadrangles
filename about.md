# Quadrangles
You need to create a program for point`s calculating in the coordinates plane for getting quadrangles.

## In the coordinates system (x,y) are 3 points A, B, and C. 
Your program should read this coordinates from the input after that the program calc coordinates next point for getting quadrangle. 
As the result, you can get one of four quadrangle's types: Trapezium, Parallelogram, Rhombus, Square, Rectangle. 
However, exists 3 varieties for next point's location (ABA'C, ABCB', and AC'BC).


## Errors
It might happen, you will get points that are in the same line, so your program should detect that and exit with the next error text (**_"Quadrangles cannot be created"_**

If input is not correct, you should detect that and write error message (**_"Input is not correct"_**). 
- Errors you should detect:
- Bad coordinates formating
- Less than 3 coordinates
- More than one or missing separator (coordinates should be writen in the format "[x, y]")

## Examples
**CHECK MORE EXAMPLES IN ../inputs.md**
**Write A:**

[0,0]

**Write B:**

[-3,4]

**Write C:**

[4,3]

A': [1,7], square

B': [7,-1], parallelogram

C': [-7,1], parallelogram
